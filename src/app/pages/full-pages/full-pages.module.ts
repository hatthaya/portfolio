import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { FormsModule } from '@angular/forms';

import { FullPagesRoutingModule } from "./full-pages-routing.module";
import { ChartistModule} from 'ng-chartist';
import { AgmCoreModule } from '@agm/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { UserProfilePageComponent } from "./user-profile/user-profile-page.component";
import { KnowledgeBaseComponent } from './knowledge-base/knowledge-base.component';
import { AboutComponent } from './about/about.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { TemplatesComponent } from './templates/templates.component';
import { SettingsComponent } from './settings/settings.component';


@NgModule({
    imports: [
        CommonModule,
        FullPagesRoutingModule,
        FormsModule,
        ChartistModule,
        AgmCoreModule,
        NgbModule
    ],
    declarations: [
        UserProfilePageComponent,
        KnowledgeBaseComponent,
        AboutComponent,
        PortfolioComponent,
        TemplatesComponent,
        SettingsComponent
    ]
})
export class FullPagesModule { }
