import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { TamplateRoutingModule } from './template-routing.module';
import { TemplateComponent } from './template.component';


@NgModule({
    imports: [
        CommonModule,
        TamplateRoutingModule,
        NgbModule,
    ],
    exports: [],
    declarations: [
        TemplateComponent
    ],
    providers: [],
})
export class TemplateModule { }
