import { Component, Inject, HostListener, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { DOCUMENT } from '@angular/platform-browser';
import { WINDOW } from '../services/windows.service';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})

export class NavbarComponent implements OnInit {
    currentLang = 'en';
    toggleClass = 'ft-maximize';
    placement = 'bottom-right'
    public gradientPurpleBlue: boolean = false;
    public isCollapsed = true;

    constructor(
        public translate: TranslateService,
        @Inject(DOCUMENT) private document: Document,
        @Inject(WINDOW) private window    ) {
        const browserLang: string = translate.getBrowserLang();
        translate.use(browserLang.match(/en|es|pt|de/) ? browserLang : 'en');   
    }
    ChangeLanguage(language: string) {
        this.translate.use(language);
    }

    ngOnInit() {
        // $.getScript('./assets/js/script.js');
        // $.getScript('./assets/js/tilt.jquery.js');
      }

    @HostListener("window:scroll", [])
  onWindowScroll() {
    const number = this.window.pageYOffset || this.document.documentElement.scrollTop || this.document.body.scrollTop || 0;
    if (number >= 60) {
      this.gradientPurpleBlue = true;
    } else {
      this.gradientPurpleBlue = false;
    }
  }
}
