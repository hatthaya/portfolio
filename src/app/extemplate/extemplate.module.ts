import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { ChartistModule } from 'ng-chartist';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatchHeightModule } from "../shared/directives/match-height.directive";

import { ExtamplateRoutingModule } from './extemlate-routing.module';
import { ExtemplateComponent } from './extemplate.component';


@NgModule({
    imports: [
        CommonModule,
        ExtamplateRoutingModule,
        ChartistModule,
        NgbModule,
        MatchHeightModule
    ],
    exports: [],
    declarations: [
        ExtemplateComponent
    ],
    providers: [],
})
export class ExtemplateModule { }
