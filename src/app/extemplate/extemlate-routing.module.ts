import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExtemplateComponent } from './extemplate.component';

const routes: Routes = [
  {
    path: '',
    component: ExtemplateComponent,
    data: {
        title: 'Extemplate'
    },

}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExtamplateRoutingModule { }
