import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtemplateComponent } from './extemplate.component';

describe('ExtemplateComponent', () => {
  let component: ExtemplateComponent;
  let fixture: ComponentFixture<ExtemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
